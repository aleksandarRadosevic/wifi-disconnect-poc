package mobile.execom.eu.wifidisconnection.service;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

public class WiFiService {

    private Context context;
    private WifiManager wifiManager;

    public WiFiService(Context context) {
        this.context = context;
        wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
    }

    public void disconnectFromCurrnetWifi() {
        if (wifiManager == null) {
            return;
        }

        wifiManager.disconnect();
    }

    public void disabledCurrentNetwork() {
        if (wifiManager == null) {
            return;
        }

        WifiInfo wifiInfo = wifiManager.getConnectionInfo();

        if (wifiInfo == null) {
            return;
        }

        wifiManager.disableNetwork(wifiInfo.getNetworkId());
    }

    public void turnOffWifi() {
        if (wifiManager == null) {
            return;
        }

        wifiManager.setWifiEnabled(false);
    }

    public void removeNetwork() {
        if (wifiManager == null) {
            return;
        }

        WifiInfo wifiInfo = wifiManager.getConnectionInfo();

        if (wifiInfo == null) {
            return;
        }

        wifiManager.removeNetwork(wifiInfo.getNetworkId());
    }
}
