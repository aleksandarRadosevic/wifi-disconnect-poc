package mobile.execom.eu.wifidisconnection;

import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import mobile.execom.eu.wifidisconnection.receiver.WifiBroadcastReceiver;
import mobile.execom.eu.wifidisconnection.service.WiFiService;

public class MainActivity extends AppCompatActivity {

    private Button disconnect, disable, disconnectAndDisable, disableAndDisconnect, remove, disconnectDisableRemove;

    private WiFiService wiFiService;

    private WifiBroadcastReceiver wifiBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        disconnect = findViewById(R.id.disconnect);
        disable = findViewById(R.id.disable);
        disconnectAndDisable = findViewById(R.id.disconnectAndDisable);
        disableAndDisconnect = findViewById(R.id.disableAndDisconnect);
        remove = findViewById(R.id.remove);
        disconnectDisableRemove = findViewById(R.id.disconnectDisableRemove);

        wiFiService = new WiFiService(this);
        wifiBroadcastReceiver = new WifiBroadcastReceiver();

        registerBroadcastReceiver();
        setupViews();
    }

    private void registerBroadcastReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
        registerReceiver(wifiBroadcastReceiver, intentFilter);
    }

    private void setupViews() {
        setupDisconnect();
        setupDisable();
        setupDisconnectAndDisable();
        setupDisableAndDisconnect();
        setupRemove();
        setupDisconnectDisableRemove();
    }

    private void setupDisconnect() {
        if (disconnect == null || wiFiService == null) {
            return;
        }

        disconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wiFiService.disconnectFromCurrnetWifi();
            }
        });
    }

    private void setupDisable() {
        if (disable == null || wiFiService == null) {
            return;
        }

        disable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wiFiService.disabledCurrentNetwork();
            }
        });
    }

    private void setupDisconnectAndDisable() {
        if (disconnectAndDisable == null || wiFiService == null) {
            return;
        }

        disconnectAndDisable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wiFiService.disconnectFromCurrnetWifi();
                wiFiService.disabledCurrentNetwork();
            }
        });
    }

    private void setupDisableAndDisconnect() {
        if (disableAndDisconnect == null || wiFiService == null) {
            return;
        }

        disableAndDisconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wiFiService.disabledCurrentNetwork();
                wiFiService.disconnectFromCurrnetWifi();
            }
        });
    }

    private void setupRemove() {
        if (remove == null || wiFiService == null) {
            return;
        }

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wiFiService.removeNetwork();
            }
        });
    }

    private void setupDisconnectDisableRemove() {
        if (disconnectDisableRemove == null || wiFiService == null) {
            return;
        }

        disconnectDisableRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wiFiService.disconnectFromCurrnetWifi();
                wiFiService.disabledCurrentNetwork();
                wiFiService.removeNetwork();
            }
        });
    }
}
